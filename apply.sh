#!/usr/bin/env bash
sass --no-source-map app.scss user.css
spicetify config current_theme spicetify-theme
spicetify config inject_css 1 replace_colors 1 overwrite_assets 1
spicetify apply
